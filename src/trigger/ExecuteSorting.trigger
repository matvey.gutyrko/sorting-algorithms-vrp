/**
 * Created by User on 16.01.2019.
 */

trigger ExecuteSorting on Input_Data__c (after insert) {
    if (Trigger.isAfter && Trigger.isInsert) {
        ExecuteSortingHandler.executeSort();
    }

}