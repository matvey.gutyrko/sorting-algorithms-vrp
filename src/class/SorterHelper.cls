/**
 * Created by User on 18.01.2019.
 */

public class SorterHelper {
    //Returns max element of array
    //n - size of listIntegers array
    public static Integer getMaxRadixSort(List<Integer> listIntegers, Integer n) {
        Integer max = listIntegers[0];
        for (Integer i = 0; i < n; i++) {
            if (listIntegers[i] > max) {
                max = listIntegers[i];
            }
        }
        return max;
    }
    public static List<Integer> countRadixSort(List<Integer> listIntegers, Integer n, Integer exp) {
        List<Integer> outputList = new List<Integer>(n);
        Integer i;
        List<Integer> count = new List<Integer>(10);
        for (Integer q = 0; q < count.size(); q++) {
            count.set(q, 0);
        }
        for (i = 0; i < n; i++) {
            count[Math.mod(listIntegers[i] / exp, 10)]++;
        }
        for (i = 1; i < 10; i++) {
            count[i] += count[i - 1];
        }
        for (i = n - 1; i >= 0; i--) {
            outputList[count[Math.mod(listIntegers[i] / exp, 10)] - 1] = listIntegers[i];
            count[Math.mod(listIntegers[i] / exp, 10)]--;
        }
        for (i = 0; i < n; i++) {
            listIntegers[i] = outputList[i];
        }
        return listIntegers;
    }
    public static List<Integer> insertionSortForTimSort(List<Integer> listIntegers, Integer left, Integer right) {
        for (Integer i = left + 1; i <= right; i++) {
            Integer temp = listIntegers[i];
            Integer j = i - 1;
            while (listIntegers[j] > temp && j >= left) {
                listIntegers[j + 1] = listIntegers[j];
                j--;
            }
            listIntegers[j + 1] = temp;
        }
        return listIntegers;
    }
    public static void mergeTimSort(List<Integer> listIntegers, Integer left, Integer mid, Integer right) {
        Integer arrLength1 = mid - left + 1;
        Integer arrLength2 = right - mid;
        Integer[] leftArr = new Integer[arrLength1];
        Integer[] rightArr = new Integer[arrLength2];
        //List<Integer> leftArr = new List<Integer>(arrLength1);
        //List<Integer> rightArr = new List<Integer>(arrLength2);
        for (Integer x = 0; x < arrLength1; x++) {
            leftArr[x] = listIntegers[left + x];
        }
        for (Integer x = 0; x < arrLength2; x++) {
            rightArr[x] = listIntegers[mid + 1 + x];
        }
        Integer i = 0;
        Integer j = 0;
        Integer k = left;
        while (i < arrLength1 && j < arrLength2) {
            if (leftArr[i] <= rightArr[j]) {
                listIntegers[k] = leftArr[i];
                i++;
            } else {
                listIntegers[k] = rightArr[j];
                j++;
            }
            k++;
        }

        while (i < arrLength1) {
            listIntegers[k] = leftArr[i];
            k++;
            i++;
        }

        while (j < arrLength2) {
            listIntegers[k] = rightArr[j];
            k++;
            j++;
        }
    }
    public static List<Integer> executeQuickSortHandler(List<Integer> arr, Integer low, Integer high) {
        if (low < high) {
            Integer pi = partition(arr, low, high);
            executeQuickSortHandler(arr, low, pi - 1);
            executeQuickSortHandler(arr, pi + 1, high);
        }
        return arr;
    }
    public static Integer partition(Integer[] arr, Integer low, Integer high) {
        Integer pivot = arr[high];
        Integer i = (low - 1); // index of smaller element
        for (Integer j = low; j < high; j++) {
            // If current element is smaller than or
            // equal to pivot
            if (arr[j] <= pivot) {
                i++;

                // swap arr[i] and arr[j]
                Integer temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }

// swap arr[i+1] and arr[high] (or pivot)
        Integer temp = arr[i + 1];
        arr[i + 1] = arr[high];
        arr[high] = temp;

        return i + 1;
    }
    public static void mergeSort(List<Integer> listIntegers, Integer l, Integer r) {
        if(l < r) {
            Integer m = (l + r) / 2;
            mergeSort(listIntegers, l, m);
            mergeSort(listIntegers, m + 1, r);
            doMerge(listIntegers, l, m, r);

        }
    }

    public static void doMerge(List<Integer> listIntegers, Integer l, Integer m, Integer r) {
        Integer n1 = m - l + 1;
        Integer n2 = r - m;

        List<Integer> left = new List<Integer>(n1);
        List<Integer> right = new List<Integer>(n2);

        for (Integer i = 0; i < n1; ++i) {
            left[i] = listIntegers[l + 1];
        }
        for (Integer j = 0; j < n2; ++j) {
            right[j] = listIntegers[m + 1 + j];
        }

        Integer i = 0;
        Integer j = 0;
        Integer k = l;

        while (i < n1 && j < n2) {
            if (left[i] <= right[j]) {
                listIntegers[k] = left[i];
                i++;
            }
            else {
                listIntegers[k] = right[j];
                j++;
            }
            k++;
        }

        while (i < n1) {
            listIntegers[k] = left[i];
            i++;
            k++;
        }

        while (j < n2) {
            listIntegers[k] = right[j];
            j++;
            k++;
        }
    }

}
