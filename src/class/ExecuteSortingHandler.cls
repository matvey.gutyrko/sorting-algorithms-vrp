/**
 * Created by User on 16.01.2019.
 */

public with sharing class ExecuteSortingHandler {
    public static void executeSort() {
        Database.executeBatch(new SortingBatch(1));
    }
}