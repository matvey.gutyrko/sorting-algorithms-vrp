/**
 * Created by User on 16.01.2019. Main
 */
public class SortingBatch implements Database.Batchable<sObject>, Database.stateful {
    public Integer counter;
    public Boolean startAnotherJob = true;
    public SortingBatch(Integer c) {
        this.counter = c;
    }
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Input_Value__c FROM Input_Data__c ORDER BY CreatedDate DESC LIMIT 1');
    }
    public void execute(Database.BatchableContext bc, List<sObject> objects) {
        Id idInput;
        for (sObject obj : objects) {
            idInput = obj.Id;
        }
        List<Integer> listIntegers = new List<Integer>();
        List<String> listValues = new List<String>();
        List<Input_Data__c> listInputData = new List<Input_Data__c>();
        for (sObject objValue : objects) {
            Input_Data__c inputValue = (Input_Data__c) objValue;
            listInputData.add(inputValue);
        }

        for (Input_Data__c inputData : listInputData) {
            listValues = inputData.Input_Value__c.deleteWhitespace().split(',');
        }

        for (String val : listValues) {
            //if (val.isNumeric()) {
                listIntegers.add(Integer.valueOf(val));
            //}
        }
        switch on counter {
            when 1 {
                Sort_Result__c sortedResult = Sorter.executeBubbleSort(listIntegers);
                sortedResult.Input_Data__c = idInput;
                if (listIntegers.size() > 0) {
                    insert sortedResult;
                } else {
                    insert new Sort_Result__c(Name = 'Bubble Sort', Result__c = 'This method does not sort strings.',
                            Input_Data__c = idInput);
                }
            }
            when 2 {
                Sort_Result__c sortedResult = Sorter.executeCocktailSort(listIntegers);
                sortedResult.Input_Data__c = idInput;
                if (listIntegers.size() > 0) {
                    insert sortedResult;
                } else {
                    insert new Sort_Result__c(Name = 'Coctail sort', Result__c = 'This method does not sort strings.',
                            Input_Data__c = idInput);
                }
            }
            when 3 {
                Sort_Result__c sortedResult = Sorter.executeRadixSort(listIntegers);
                sortedResult.Input_Data__c = idInput;
                if (listIntegers.size() > 0) {
                    insert sortedResult;
                } else { 
                    insert new Sort_Result__c(Name = 'Radix sort', Result__c = 'This method does not sort strings.',
                            Input_Data__c = idInput);
                }
            }
            when 4 {
                Sort_Result__c sortedResult = Sorter.executeGnomeSort(listIntegers);
                sortedResult.Input_Data__c = idInput;
                if (listIntegers.size() > 0) {
                    insert sortedResult;
                } else {
                    insert new Sort_Result__c(Name = 'Gnome sort', Result__c = 'This method does not sort strings.',
                            Input_Data__c = idInput);
                }
            }
            when 5 {
                Sort_Result__c sortedResult = Sorter.executeInsertionSort(listIntegers);
                sortedResult.Input_Data__c = idInput;
                if (listIntegers.size() > 0) {
                    insert sortedResult;
                } else {
                    insert new Sort_Result__c(Name = 'Insertion sort', Result__c = 'This method does not sort strings.',
                            Input_Data__c = idInput);
                }
            }
            when 6 {
                Sort_Result__c sortedResult = Sorter.executeApexSortIntegers(listIntegers);
                sortedResult.Input_Data__c = idInput;
                if (listIntegers.size() > 0) {
                    insert sortedResult;
                } else {
                    insert new Sort_Result__c(Name = 'Apex Sort Integers', Result__c = 'This method does not sort strings.',
                            Input_Data__c = idInput);
                }
            }
            when 7 {
                Sort_Result__c sortedResult = Sorter.executeSelectionSort(listIntegers);
                sortedResult.Input_Data__c = idInput;
                if (listIntegers.size() > 0) {
                    insert sortedResult;
                } else {
                    insert new Sort_Result__c(Name = 'Selection sort ', Result__c = 'This method does not sort strings.',
                            Input_Data__c = idInput);
                }
            }
            when 8 {
                Sort_Result__c sortedResult = Sorter.executeQuickSort(listIntegers);
                sortedResult.Input_Data__c = idInput;
                if (listIntegers.size() > 0) {
                    insert sortedResult;
                } else {
                    insert new Sort_Result__c(Name = 'Quick Sort ', Result__c = 'This method does not sort strings.',
                            Input_Data__c = idInput);
                }
            }
            when 9 {
                Sort_Result__c sortedResult = Sorter.executeShellSort(listIntegers);
                sortedResult.Input_Data__c = idInput;
                if (listIntegers.size() > 0) {
                    insert sortedResult;
                } else {
                    insert new Sort_Result__c(Name = 'Shell Sort', Result__c = 'This method does not sort strings.',
                            Input_Data__c = idInput);
                }
            }
        }
//
        if (counter == 9) {
            startAnotherJob = false;
        } else {
            counter++;
        }
    }
    public void finish(Database.BatchableContext bc) {
        if (startAnotherJob) {
            Database.executeBatch(new SortingBatch(counter));
        } else {
            System.debug('Start method signalled that no need to chain another batch another batch job');
        }
    }
}