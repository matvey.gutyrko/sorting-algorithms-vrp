/**
 * Created by User on 16.01.2019.
 */

public with sharing class CheckInputValueHandler {
    public static void checkInput(List<Input_Data__c> inputValues) {
        List<String> listValues = new List<String>();
        List<Integer> listIntegers = new List<Integer>();
        List<String> listStrings = new List<String>();
        for (Input_Data__c value : inputValues) {
            listValues = value.Input_Value__c.deleteWhitespace().split(',');
        }
        for (String str : listValues) {
            if (str.isNumeric()) {
                listIntegers.add(Integer.valueOf(str));
            } else {
                listStrings.add(str);
            }
        }
        if (listStrings.size() > 0 && listIntegers.size() > 0) {
            for (Input_Data__c err : inputValues) {
                err.addError('Error, just enter string or positive number');
            }
        }
    }
}