/**
 * Created by User on 16.01.2019.
 */
//COMMITEDhbghj
public with sharing class Sorter {
    //Bubble sort integer
    public static final Integer RUN = 32;
    public static Sort_Result__c executeBubbleSort(List<Integer> listValues) {
        Integer size = listValues.size();
        Long startTime = Datetime.now().getTime();
        for (Integer i = 0; i < size - 1; i++) {
            for (Integer j = 0; j < size - i - 1; j++) {
                if (listValues[j] > listValues[j + 1]) {
                    Integer temp = listValues[j];
                    listValues[j] = listValues[j + 1];
                    listValues[j + 1] = temp;
                }
            }
        }
        Long endTime = Datetime.now().getTime();
        Long timeResult = endTime - startTime;
        String strResult = (String) JSON.serialize(listValues);
        return new Sort_Result__c(Name = 'Bubble sort', Result__c = strResult, Execute_Time__c = String.valueOf(timeResult));
    }

    //Coctail sort https://www.geeksforgeeks.org/cocktail-sort/
    public static Sort_Result__c executeCocktailSort(List<Integer> listValues) {

        boolean swapped = true;
        Integer start = 0;
        Integer endArr = listValues.size();
        Long startTime = Datetime.now().getTime();
        while (swapped == true) {
            swapped = false;
            for (Integer i = start; i < endArr - 1; ++i) {
                if (listValues[i] > listValues[i + 1]) {
                    Integer temp = listValues[i];
                    listValues[i] = listValues[i + 1];
                    listValues[i + 1] = temp;
                    swapped = true;
                }
            }

            if (swapped == false)
                break;
            swapped = false;
            endArr = endArr - 1;
            for (Integer i = endArr - 1; i >= start; i--) {
                if (listValues[i] > listValues[i + 1]) {
                    Integer temp = listValues[i];
                    listValues[i] = listValues[i + 1];
                    listValues[i + 1] = temp;
                    swapped = true;
                }
            }
            start = start + 1;
        }
        Long endTime = Datetime.now().getTime();
        Long timeResult = endTime - startTime;
        String strResult = (String) JSON.serialize(listValues);
        return new Sort_Result__c(Name = 'Cocktail sort', Result__c = strResult, Execute_Time__c = String.valueOf(timeResult));
    }
    //Insertion sort https://www.geeksforgeeks.org/insertion-sort/
    //fixed
    public static Sort_Result__c executeInsertionSort(List<Integer> listValues) {
        Integer i, key, j;
        Integer n = listValues.size();
        Long startTime = Datetime.now().getTime();
        for (i = 1; i < n; i++) {
            key = listValues[i];
            j = i - 1;
            while (j >= 0 && listValues[j] > key) {
                listValues[j + 1] = listValues[j];
                j = j - 1;
            }
            listValues[j + 1] = key;
        }
        Long endTime = Datetime.now().getTime();
        Long timeResult = endTime - startTime;
        String strResult = (String) JSON.serialize(listValues);
        return new Sort_Result__c(Name = 'Insertion sort', Result__c = strResult, Execute_Time__c = String.valueOf(timeResult));
    }

    //Gnome sort https://www.geeksforgeeks.org/gnome-sort-a-stupid-one/
    public static Sort_Result__c executeGnomeSort(List<Integer> listValues) {
        Integer index = 0;
        Integer n = listValues.size();
        Long startTime = Datetime.now().getTime();
        while (index < n) {
            if (index == 0)
                index++;
            if (listValues[index] >= listValues[index - 1])
                index++; else {
                Integer temp = 0;
                temp = listValues[index];
                listValues[index] = listValues[index - 1];
                listValues[index - 1] = temp;
                index--;
            }
        }
        Long endTime = Datetime.now().getTime();
        Long timeResult = endTime - startTime;
        String strResult = (String)JSON.serialize(listValues);

        return new Sort_Result__c(Name = 'Gnome sort', Result__c = strResult, Execute_Time__c = String.valueOf(timeResult));
    }
    //TODO: тоже не рабоатет
    //Merge sort https://www.geeksforgeeks.org/merge-sort/
    public static void executeMergeSort(List<Integer> listIntegers) {
        SorterHelper.mergeSort(listIntegers, 0, listIntegers.size() - 1);
        System.debug(listIntegers);
    }
    //TODO: блядина не хочет работать
    //Timsort https://www.geeksforgeeks.org/timsort/
    public static Sort_Result__c executeTimSort(List<Integer> listIntegers) {
        List<Integer> outputList = new List<Integer>();
        Integer size = listIntegers.size();

        Long startTime = Datetime.now().getTime();
        for (Integer i = 0; i < size; i += RUN) {
            SorterHelper.insertionSortForTimSort(listIntegers, i, Math.min((i + 31), (size - 1)));
        }
        for (Integer j = RUN; j < size; j = 2 * j) {
            for (Integer left = 0; left < size; left += 2 * j) {
                Integer mid = left + j - 1;
                Integer right = Math.min((left + 2 * j - 1), (size - 1));
                SorterHelper.mergeTimSort(listIntegers, left, mid, right);
            }

        }
        Long endTime = Datetime.now().getTime();
        Long timeResult = endTime - startTime;
        String strResult = (String)JSON.serialize(listIntegers);
        return new Sort_Result__c(Name = 'Tim sort', Result__c = strResult, Execute_Time__c = String.valueOf(timeResult));
    }
    //Radixsort https://www.geeksforgeeks.org/radix-sort/
    public static Sort_Result__c executeRadixSort(List<Integer> listValues) {
        Integer size = listValues.size();
        Integer max = SorterHelper.getMaxRadixSort(listValues, size);
        List<Integer> output = new List<Integer>();
        Long startTime = Datetime.now().getTime();
        for (Integer exp = 1; max / exp > 0; exp *= 10) {
            output = SorterHelper.countRadixSort(listValues, size, exp);
        }
        Long endTime = Datetime.now().getTime();
        Long timeResult = endTime - startTime;
        String strResult = (String) JSON.serialize(output);
        return new Sort_Result__c(Name = 'Radix sort', Result__c = strResult, Execute_Time__c = String.valueOf(timeResult));
    }
    //Bucket sort https://www.geeksforgeeks.org/bucket-sort-2/
    public static String executeBucketSort() {
        return null;
    }
    //Counting sort https://www.geeksforgeeks.org/counting-sort/
    public static String executeCountingSort() {
        return null;
    }
    public static Sort_Result__c executeApexSortIntegers(List<Integer> listValues) {
        Long startTime = Datetime.now().getTime();
        listValues.sort();
        Long endTime = Datetime.now().getTime();
        Long timeResult = endTime - startTime;
        String strResult = (String) JSON.serialize(listValues);
        return new Sort_Result__c(Name = 'Apex .sort() Integers', Result__c = strResult, Execute_Time__c = String.valueOf(timeResult));
    }
    public static Sort_Result__c executeShellSort(List<Integer>listValues) {
        Integer n = listValues.size();
        Long startTime = Datetime.now().getTime();
        for (Integer gap = n / 2; gap > 0; gap /= 2) {
            for (Integer i = gap; i < n; i += 1) {
                Integer temp = listValues[i];
                Integer j;
                for (j = i; j >= gap && listValues[j - gap] > temp; j -= gap)
                    listValues[j] = listValues[j - gap];
                listValues[j] = temp;
            }
        }
        Long endTime = Datetime.now().getTime();
        Long timeResult = endTime - startTime;
        String strResult = (String) JSON.serialize(listValues);
        return new Sort_Result__c(Name = 'Shell Sort', Result__c = strResult, Execute_Time__c = String.valueOf(timeResult));
    }
    //
    public static Sort_Result__c executeQuickSort(List<Integer>listValues) {
        Integer n = listValues.size();
        Long startTime = Datetime.now().getTime();
        List<Integer> output = SorterHelper.executeQuickSortHandler(listValues, 0, n-1);
        Long endTime = Datetime.now().getTime();
        Long timeResult = endTime - startTime;
        String strResult = (String)JSON.serialize(output);
        return new Sort_Result__c(Name = 'Quick Sort', Result__c = strResult, Execute_Time__c = String.valueOf(timeResult));
    }
    //selection sort
    public static Sort_Result__c executeSelectionSort(List<Integer> listIntegers) {
        Long startTime = Datetime.now().getTime();
        for (Integer i = 0; i < listIntegers.size() - 1; i++) {
            Integer index = i;
            for (Integer j = i + 1; j < listIntegers.size(); j++) {
                if (listIntegers[j] < listIntegers[index]) {
                    index = j;
                }
                Integer smallerNumber = listIntegers[index];
                listIntegers[index] = listIntegers[i];
                listIntegers[i] = smallerNumber;
            }
        }
        Long endTime = Datetime.now().getTime();
        Long timeResult = endTime - startTime;
        String strResult = (String)JSON.serialize(listIntegers);
        return new Sort_Result__c(Name = 'Selection sort', Result__c = strResult, Execute_Time__c = String.valueOf(timeResult));
    }
}