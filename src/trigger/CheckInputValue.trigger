/**
 * Created by User on 16.01.2019.
 */

trigger CheckInputValue on Input_Data__c (before insert) {
    if (Trigger.isInsert && Trigger.isBefore) {
        CheckInputValue.checkInput(Trigger.new);
    }
}